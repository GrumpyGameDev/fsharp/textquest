namespace TextQuest.Runner

open TextQuest.Engine
open System

module Runner=
    let rec private confirm (prompt:string): bool =
        prompt
        |> printfn "\n%s"
        printfn "[Y]es"
        printfn "[N]o"

        match Console.ReadKey(true).Key with
        | ConsoleKey.Y -> 
            true
        | ConsoleKey.N -> 
            false
        | _ -> 
            confirm prompt

    let private handleSuccessResult (command:Command) : unit =
        match command with
        | NewAvatar ->
            printfn "\nSuccessfully created a new avatar."
        | MoveAvatar direction ->
            printfn "\nYou move %s." (direction.ToString())
        | TurnAvatar direction ->
            printfn "\nYou turn %s." (direction.ToString())
        | x ->
            (x.ToString()) |> printfn "\nSUCCESS: %s"

    let private handleFailureResult (command:Command) : unit =
        match command with
        | NewAvatar ->
            printfn "\nFailed to create a new avatar."
        | x ->
            (x.ToString()) |> printfn "\nFAILURE: %s"

    let private handleAvatarInventoryAddResult (item:Item) (amount:uint32) : unit =
        printfn "\nYou acquire %u %s." amount (item.ToString())

    let private handleAvatarInventoryRemoveResult (item:Item) (amount:uint32) : unit =
        printfn "\nYou lose %u %s." amount (item.ToString())

    let private handleAreaInventoryAddResult (item:Item) (amount:uint32) : unit =
        printfn "\n%u %s appear on the ground." amount (item.ToString())

    let private handleAreaInventoryRemoveResult (item:Item) (amount:uint32) : unit =
        printfn "\n%u %s disappear from the ground." amount (item.ToString())

    let private handleAreaTerrainChangeResult (terrain:Terrain) : unit =
        printfn "\nArea terrain changed to %s." (terrain.ToString())

    let private handleAvatarEquipSlotChange (slot:EquipSlot) (item:Item option) : unit =
        printfn "\nYou %s %s %s your %s." (slot |> EquipSlot.verb) (item |> Option.map (fun x -> x.ToString()) |> Option.defaultValue "nothing") (slot |> EquipSlot.preposition) (slot.ToString())

    let private handleResult (result:Result) : unit =
        match result with
        | Success command ->
            command
            |> handleSuccessResult
        | Failure command ->
            command
            |> handleFailureResult
        | AvatarInventoryAdd (item,amount)->
            handleAvatarInventoryAddResult item amount
        | AvatarInventoryRemove (item,amount)->
            handleAvatarInventoryRemoveResult item amount
        | AreaInventoryAdd (item,amount)->
            handleAreaInventoryAddResult item amount
        | AreaInventoryRemove (item,amount)->
            handleAreaInventoryRemoveResult item amount
        | AreaTerrainChange terrain ->
            handleAreaTerrainChangeResult terrain
        | AvatarEquipSlotChange (slot, item) ->
            handleAvatarEquipSlotChange slot item
         


    let private handleExit (engine:Engine) : Engine option =
        if confirm "Are you sure you want to exit?" then
            None
        else
            engine |> Some

    let private doCommand = Engine.doCommand handleResult

    let rec private handleTurnAvatar (engine:Engine) : Engine =
        printfn "\nTurn which way?"
        printfn "\n[L]eft\n[R]ight\n[A]round\n[Esc]-Cancel"
        match Console.ReadKey(true).Key with
        | ConsoleKey.L ->
            engine
            |> doCommand (TurnAvatar TurnDirection.Left)
        | ConsoleKey.R ->
            engine
            |> doCommand (TurnAvatar TurnDirection.Right)
        | ConsoleKey.A ->
            engine
            |> doCommand (TurnAvatar Around)
        | ConsoleKey.Escape ->
            printfn "\nTurn canceled"
            engine
        | _ ->
            handleTurnAvatar engine

    let rec private handleMoveAvatar (engine:Engine) : Engine =
        printfn "\nMove which way?"
        printfn "\n[A]head\n[L]eft\n[R]ight\n[B]ack\n[Esc]-Cancel"
        match Console.ReadKey(true).Key with
        | ConsoleKey.L ->
            engine
            |> doCommand (MoveAvatar Left)
        | ConsoleKey.R ->
            engine
            |> doCommand (MoveAvatar Right)
        | ConsoleKey.A ->
            engine
            |> doCommand (MoveAvatar Ahead)
        | ConsoleKey.B ->
            engine
            |> doCommand (MoveAvatar Back)
        | ConsoleKey.Escape ->
            printfn "\nMove canceled"
            engine
        | _ ->
            handleMoveAvatar engine

    let private displayHome(avatar:Avatar) : unit =
        match avatar.home with
        | Some l ->
            let delta = Location.delta avatar.location l
            let adjustedDelta =
                match avatar.facing with
                | North -> delta
                | East -> delta |> Location.rotateCounterClockwise
                | South -> delta |> Location.rotateClockwise |> Location.rotateClockwise
                | West -> delta |> Location.rotateClockwise
            match adjustedDelta.x, adjustedDelta.y with
            | x, 0 when x>0 ->
                printfn "\nHome is directly to your right."
            | x, 0 when x<0 ->
                printfn "\nHome is directly to your left."
            | 0, y when y>0 ->
                printfn "\nHome is directly ahead of you."
            | 0, y when y<0 ->
                printfn "\nHome is directly behind you."
            | x, y when x<0 && y<0 ->
                printfn "\nHome is behind you and to your left."
            | x, y when x<0 && y>0 ->
                printfn "\nHome is ahead of you and to your left."
            | x, y when x>0 && y<0 ->
                printfn "\nHome is ahed of you and to your right."
            | x, y when x>0 && y>0 ->
                printfn "\nHome is behind you and to your right."
            | _ -> 
                printfn "\nYou are at home."
        | None ->
            printfn "\nYou have no home set."

    let rec private handleHome (avatar:Avatar) (engine:Engine) : Engine =
        displayHome(avatar)
        printfn "\n[S]et to current location\n[C]lear home position\n[Esc]-Cancel"
        match Console.ReadKey(true).Key with
        | ConsoleKey.S ->
            if confirm "Are you sure you want to set your home location?" then
                engine
                |> doCommand SetAvatarHome
            else
                handleHome avatar engine
        | ConsoleKey.C ->
            if confirm "Are you sure you want to clear your home location?" then
                engine
                |> doCommand ClearAvatarHome
            else
                handleHome avatar engine
        | ConsoleKey.Escape ->
            engine
        | _ ->
            handleHome avatar engine

    let private handleInteract (engine:Engine) : Engine =
        engine
        |> doCommand Interact

    let private handleDropInventory (table:Map<uint32,Item>) (engine:Engine) : Engine =
        printf "\nItem Index>"
        match System.UInt32.TryParse(System.Console.ReadLine()) with
        | true, x when table.ContainsKey x->
            printf "\nHow many?"
            match System.UInt32.TryParse(System.Console.ReadLine()) with
            | true, y when y > 0u ->
                engine
                |> doCommand (AvatarDrop (table.[x],y))
            | _ ->
                engine
        | _ ->
            engine

    let rec private handleInventory (avatar:Avatar) (engine:Engine) : Engine =
        printfn "\nInventory:"
        if avatar.inventory |> Map.isEmpty then
            printfn "(empty)"
            engine
        else
            let table = 
                avatar.inventory
                |> Map.fold 
                    (fun (index:uint32, tbl:Map<uint32, Item>) (k:Item) (v:uint32) -> 
                        printfn "%u) %s %u" index (k.ToString()) v
                        (index+1u,tbl |> Map.add index k)) (1u,Map.empty)
                |> snd
            printfn "\n[D]rop\n[Esc]-Cancel"
            match Console.ReadKey(true).Key with
            | ConsoleKey.D ->
                let newEngine =
                    engine
                    |> handleDropInventory table
                handleInventory newEngine.avatar.Value newEngine
            | ConsoleKey.Escape ->
                engine
            | _ ->
                handleInventory avatar engine

    let rec private handleCraft (terrain:Terrain) (avatar:Avatar) (engine:Engine) : Engine =
        printfn "\nRecipes Available:"
        match Recipe.getAvailableRecipes terrain avatar.inventory with
        | [] ->
            printfn "(none)"
            engine
        | recipes ->
            let table = 
                ([1..(recipes.Length)], recipes)
                ||> List.zip  
                |> Map.ofList
            table
            |> Map.iter
                (fun k v ->
                    printfn "%d %s" k (v.ToString()))
            match Console.ReadLine() |> Int32.TryParse with
            | false, _ ->
                printfn "Crafting canceled!"
                engine
            | true, index ->
                table
                |> Map.tryFind index
                |> Option.fold
                    (fun a i -> 
                        a
                        |> doCommand (i |> AvatarCraft)) engine


    let private handleTakeGround (table:Map<uint32,Item>) (engine:Engine) : Engine =
        printf "\nItem Index>"
        match System.UInt32.TryParse(System.Console.ReadLine()) with
        | true, x when table.ContainsKey x->
            printf "\nHow many?"
            match System.UInt32.TryParse(System.Console.ReadLine()) with
            | true, y when y > 0u ->
                engine
                |> doCommand (AvatarTake (table.[x],y))
            | _ ->
                engine
        | _ ->
            engine

    let rec private handleGround (engine:Engine) : Engine =
        let area = engine |> Engine.getAvatarArea
        printfn "\nOn Ground:"
        if area.inventory |> Map.isEmpty then
            printfn "(empty)"
            engine
        else
            let table = 
                area.inventory
                |> Map.fold 
                    (fun (index:uint32, tbl:Map<uint32, Item>) (k:Item) (v:uint32) -> 
                        printfn "%u) %s %u" index (k.ToString()) v
                        (index+1u,tbl |> Map.add index k)) (1u,Map.empty)
                |> snd
            printfn "\n[T]ake\n[Esc]-Cancel"
            match Console.ReadKey(true).Key with
            | ConsoleKey.T ->
                let newEngine =
                    engine
                    |> handleTakeGround table
                handleGround newEngine
            | ConsoleKey.Escape ->
                engine
            | _ ->
                handleGround engine



    let rec private handleEquip (engine:Engine) : Engine =
        match engine.avatar with
        | Some avatar ->
            printfn "Currently Equipped:"
            let table = 
                EquipSlot.slotList
                |> List.zip [1..(EquipSlot.slotList.Length)]
                |> List.map
                    (fun (index, slot) -> (index, slot))
                |> Map.ofList
            table
            |> Map.iter
                (fun index slot ->
                    printfn "%d) %s: %s" index (slot.ToString()) (avatar.equipped |> Map.tryFind slot |> Option.map (fun x -> x.ToString()) |> Option.defaultValue "(nothing)"))
            printf "Which slot (blank for none)?"
            match Console.ReadLine() |> Int32.TryParse with
            | false, _ ->
                printfn "Equip operation canceled."
                engine
            | true, index ->
                table
                |> Map.tryFind index
                |> Option.fold
                    (fun e slot -> 
                        let items =
                            avatar.inventory
                            |> Map.toList
                            |> List.map fst
                            |> List.filter
                                (Item.isEquippable slot)
                        let itemTable = 
                            items
                            |> List.zip [1..(items.Length)]
                            |> Map.ofList
                        printfn "Which item to equip (blank for none)?"
                        itemTable
                        |> Map.iter 
                            (fun k v -> 
                                printfn "%d %s" k (v.ToString()))
                        match Console.ReadLine() |> Int32.TryParse with
                        | false, _ ->
                            e
                            |> doCommand ((slot, None) |> AvatarEquip)
                        | true, index ->
                            e
                            |> doCommand ((slot, itemTable |> Map.tryFind index) |> AvatarEquip)) engine
        | _ ->
            engine

    let private handlePlace (engine:Engine) : Engine =
        match engine.avatar with
        | Some avatar ->
            let terrain = 
                engine
                |> Engine.getAvatarArea
                |> Area.getTerrain 
            let canPlace = 
                avatar.equipped
                |> Map.tryFind OnHand
                |> Option.map (fun i -> Terrain.canPlace i terrain)
                |> Option.defaultValue false
            if canPlace then
                engine
            else
                printfn "\nYou cannot place that here."
                engine
                |> doCommand AvatarPlace
        | None ->
            engine


    let private handleAvatar (avatar:Avatar) (engine:Engine) : Engine option=
        let area = engine |> Engine.getAvatarArea
        printfn "\n==============================================================="
        printfn "Terrain: %s" (area.terrain.ToString())
        printfn "\n[I]nventory\n[G]round\nInt[E]ract\n[C]raft\nE[Q]uip\n[M]ove\n[T]urn\n[H]ome\nE[X]it"
        match Console.ReadKey(true).Key with
        | ConsoleKey.G ->
            engine
            |> handleGround
            |> Some

        | ConsoleKey.I ->
            engine
            |> handleInventory avatar
            |> Some

        | ConsoleKey.C ->
            engine
            |> handleCraft area.terrain avatar
            |> Some

        | ConsoleKey.E ->
            engine
            |> handleInteract
            |> Some

        | ConsoleKey.H ->
            engine
            |> handleHome avatar
            |> Some

        | ConsoleKey.T ->
            engine
            |> handleTurnAvatar
            |> Some

        | ConsoleKey.M ->
            engine
            |> handleMoveAvatar
            |> Some

        | ConsoleKey.X ->
            engine
            |> handleExit

        | ConsoleKey.Q ->
            engine
            |> handleEquip
            |> Some

        | ConsoleKey.P ->
            engine
            |> handlePlace
            |> Some


        | _ ->
            engine 
            |> Some

    let private handleNoAvatar (engine:Engine) : Engine option=
        printfn "\nNo Avatar."
        printfn "\n[N]ew avatar\nE[x]it"
        match Console.ReadKey(true).Key with
        | ConsoleKey.N ->
            engine
            |> doCommand NewAvatar
            |> Some
        | ConsoleKey.X ->
            engine
            |> handleExit
        | _ -> 
            engine 
            |> Some

    let rec run (engine:Engine) : unit =
        let newEngine = 
            match engine.avatar with
            | Some avatar ->
                engine
                |> handleAvatar avatar

            | None ->
                engine
                |> handleNoAvatar
        match newEngine with
        | Some e ->
            run e
        | None ->
            ()
