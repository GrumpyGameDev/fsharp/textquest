﻿open TextQuest.Engine
open TextQuest.Runner

[<EntryPoint>]
let main _ =
    Engine.create()
    |> Runner.run
    0
