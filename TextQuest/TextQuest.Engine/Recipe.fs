namespace TextQuest.Engine

type Recipe =
    | Workbench
    | WoodAxe
    override x.ToString() =
        match x with 
        | Workbench -> "workbench"
        | WoodAxe -> "wood axe"
        

module Recipe =
    let private inputs: Map<Recipe,Map<Item,uint32>> =
        [
            (Workbench, [Wood, 4u] |> Map.ofList)
            (WoodAxe, [Wood, 5u] |> Map.ofList)
        ]
        |> Map.ofList

    let private outputs: Map<Recipe,Map<Item,uint32>> =
        [
            (Workbench, [Item.Workbench, 1u] |> Map.ofList)
            (WoodAxe, [Axe, 25u] |> Map.ofList)
        ]
        |> Map.ofList

    let getInputs (recipe:Recipe) : Map<Item, uint32> =
        inputs
        |> Map.tryFind recipe
        |> Option.defaultValue Map.empty

    let getOutputs (recipe:Recipe) : Map<Item, uint32> =
        outputs
        |> Map.tryFind recipe
        |> Option.defaultValue Map.empty

    let validateTerrain (terrain:Terrain) (recipe:Recipe) : bool =
        match recipe with
        | WoodAxe ->
            terrain
            |> Terrain.isWorkbench
        | _ -> 
            true

    let private recipes =
        [
            Workbench
            WoodAxe
        ]

    let internal canCraft (terrain:Terrain) (inventory:Map<Item,uint32>) (recipe:Recipe) : bool =
        if recipe |> validateTerrain terrain then
            recipe
            |> getInputs
            |> Map.forall
                (fun k v -> 
                    (inventory |> Map.tryFind k |> Option.defaultValue 0u) >= v)
        else
            false

    let getAvailableRecipes (terrain:Terrain) (inventory:Map<Item,uint32>) : List<Recipe> =
        recipes
        |> List.filter
            (canCraft terrain inventory)
