namespace TextQuest.Engine

type Area =
    {
        terrain: Terrain
        inventory: Map<Item,uint32>
    }

module Area=
    let create (random:System.Random) : Area =
        {
            terrain = Terrain.create random
            inventory = Map.empty
        }

    let internal getInventory (item:Item) (area:Area) : uint32 =
        match area.inventory |> Map.tryFind item with
        | Some x -> x
        | None -> 0u

    let private setInventory (item:Item) (amount:uint32) (area:Area) : Area =
        if amount > 0u then
            {area with inventory = area.inventory |> Map.add item amount}
        else
            {area with inventory = area.inventory |> Map.remove item}

    let internal addInventory (item:Item) (amount:uint32) (area:Area) : Area =
        area
        |> setInventory item (amount + (area |> getInventory item))

    let internal removeInventory (item:Item) (amount:uint32) (area:Area) : Area =
        let current = (area |> getInventory item) 
        if amount >= current then
            area |> setInventory item 0u
        else
            area |> setInventory item (current - amount)

    let internal setTerrain (terrain:Terrain) (area:Area) : Area =
        {area with terrain = terrain}

    let private interactTree (resultHandler:Result->unit) (descriptor:TreeDescriptor) (avatar:Avatar) (area:Area) : Avatar * Area =
        let chopRate, item = 
            match avatar.equipped |> Map.tryFind OnHand with
            | Some Axe ->
                2u, Some Axe
            | _ ->
                1u, None
        let chopped = min chopRate descriptor.woodLeft
        let newArea =
            match descriptor.woodLeft - chopped with
            | 0u -> 
                resultHandler (AreaTerrainChange Stump)                
                area 
                |> setTerrain Stump
            | x -> 
                area 
                |> setTerrain (descriptor |> TreeDescriptor.setWoodLeft x |> Tree)
        resultHandler (AvatarInventoryAdd (Wood,chopped))
        let newAvatar = 
            avatar 
            |> Avatar.addInventory Wood chopped
            |> Option.foldBack 
                (fun i a -> 
                    a
                    |> Avatar.removeInventory i 1u) item
        (newAvatar, newArea)

    let internal interact (resultHandler:Result->unit) (avatar:Avatar) (area:Area) : Avatar * Area =
        match area.terrain with
        | Tree t ->
            interactTree resultHandler t avatar area
        | _ ->       
            (avatar, area)

    let internal drop  (resultHandler:Result->unit) (item:Item) (amount:uint32) (avatar:Avatar) (area:Area) : Avatar * Area =
        match min amount (avatar |> Avatar.getInventory item) with
        | 0u ->
            (avatar, area)
        | dropped ->
            (item, dropped) |> AvatarInventoryRemove |> resultHandler
            (item, dropped) |> AreaInventoryAdd |> resultHandler
            let newAvatar =
                avatar 
                |> Avatar.removeInventory item dropped
            let newArea =
                area
                |> addInventory item dropped
            (newAvatar, newArea)

    let internal take  (resultHandler:Result->unit) (item:Item) (amount:uint32) (avatar:Avatar) (area:Area) : Avatar * Area =
        match min amount (area |> getInventory item) with
        | 0u ->
            (avatar, area)
        | taken ->
            (item, taken) |> AvatarInventoryAdd |> resultHandler
            (item, taken) |> AreaInventoryRemove |> resultHandler
            let newAvatar =
                avatar 
                |> Avatar.addInventory item taken
            let newArea =
                area
                |> removeInventory item taken
            (newAvatar, newArea)

    let getTerrain (area:Area) : Terrain =
        area.terrain
