namespace TextQuest.Engine

type Item =
    | Wood
    | Workbench
    | Axe
    override x.ToString(): string =
        match x with
        | Wood -> "wood"
        | Workbench -> "workbench"
        | Axe -> "axe"

module Item =
    let private equippables = 
        [
            (OnHand, Workbench)
            (OnHand, Axe)
        ]
        |> Set.ofList

    let isEquippable (slot:EquipSlot) (item:Item) : bool =
        equippables.Contains(slot,item)