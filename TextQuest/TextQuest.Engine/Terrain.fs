namespace TextQuest.Engine

type Terrain =
    | Grass
    | Tree of TreeDescriptor
    | Stump
    | Workbench of Terrain
    override x.ToString() : string =
        match x with
        | Grass -> "grass"
        | Tree _-> "woods"
        | Workbench t -> sprintf "workbench on %s" (t.ToString())
        | Stump -> "stump"

module Terrain =
    let private table =
        [
            (Grass,80000u)
            (Tree TreeDescriptor.defaultValue,20000u)
        ]
        |> Map.ofList

    let internal create (random:System.Random) : Terrain =
        match Utility.generate random table with
        | Tree _ ->
            Tree (TreeDescriptor.create random)
        | x -> x

    let internal isWorkbench (terrain:Terrain) : bool =
        match terrain with
        | Workbench _ ->
            true
        | _ ->
            false

    let canPlace (item:Item) (terrain:Terrain) : bool =
        match terrain, item with
        | Grass, Item.Workbench ->
            true
        | _, _ ->
            false

    let internal place (item:Item) (terrain:Terrain) : Terrain option =
        match terrain, item with
        | Grass, Item.Workbench ->
            Workbench terrain
            |> Some
        | _, _ ->
            None


