namespace TextQuest.Engine

type EquipSlot =
    | OnHand
    override x.ToString() =
        match x with 
        | OnHand -> "dominant hand"

module EquipSlot =
    let slotList =
        [
            OnHand
        ]

    let verb (slot:EquipSlot) : string =
        match slot with
        | OnHand -> "wield"

    let preposition (slot:EquipSlot) : string =
        match slot with
        | OnHand -> "in"