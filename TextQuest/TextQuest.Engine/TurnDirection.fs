﻿namespace TextQuest.Engine

type TurnDirection = 
    | Left
    | Right
    | Around
    override x.ToString() =
        match x with
        | Left -> "left"
        | Right -> "right"
        | Around -> "around"

