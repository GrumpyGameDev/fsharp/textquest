namespace TextQuest.Engine

type Location =
    {
        x:int
        y:int
    }

module Location =
    let internal nextLocation (direction:Direction) (location:Location) : Location =
        match direction with
        | North -> {location with y=location.y+1}
        | East -> {location with x=location.x+1}
        | South -> {location with y=location.y-1}
        | West -> {location with x=location.x-1}

    let delta (from:Location) (``to``:Location) : Location =
        {
            x = ``to``.x - from.x
            y = ``to``.y - from.y
        }

    let rotateClockwise (location:Location) : Location =
        {
            x = location.y
            y = -location.x
        }

    let rotateCounterClockwise = rotateClockwise >> rotateClockwise >> rotateClockwise