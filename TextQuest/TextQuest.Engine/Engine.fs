﻿namespace TextQuest.Engine

type Engine =
    {
        random: System.Random
        avatar: Avatar option
        areas: Map<Location,Area>
    }

module Engine =
    let create () : Engine =
        let random = System.Random()
        {
            random = random
            avatar = None
            areas = Map.empty
        }

    let private tryGetArea (location:Location) (engine:Engine): Area option =
        engine.areas 
        |> Map.tryFind location

    let private tryGetAvatarArea (engine:Engine) : Area option =
        match engine.avatar with
        | Some a ->
            engine
            |> tryGetArea a.location
        | None ->
            None

    let getAvatarArea = tryGetAvatarArea >> Option.get

    let private setArea(location:Location) (area:Area) (engine:Engine) : Engine =
        {engine with areas = engine.areas |> Map.add location area}

    let private updateArea (updater:Area option -> Area option) (location:Location) (engine:Engine) : Engine =
        let newArea = 
            engine.areas
            |> Map.tryFind location
            |> updater
        match newArea with
        | Some area ->
            {engine with areas = engine.areas |> Map.add location area}
        | None ->
            {engine with areas = engine.areas |> Map.remove location}

    let private updateAvatarArea (updater:Area option -> Area option) (engine:Engine) : Engine =
        engine.avatar
        |> Option.map Avatar.getLocation
        |> Option.fold 
            (fun e l -> 
                e
                |> updateArea updater l) engine
        
    let private generateAvatarArea (engine:Engine): Engine =
        match engine.avatar with
        | Some a ->
            match engine |> tryGetAvatarArea with
            | Some _ ->
                engine
            | None ->
                engine
                |> setArea a.location (Area.create(engine.random))
        | None ->
            engine

    let private setAvatar (avatar:Avatar option) (engine:Engine) : Engine =
        {engine with avatar = avatar}

    let private doNewAvatarCommand (resultHandler:Result->unit) (engine:Engine) : Engine =
        match engine.avatar with
        | Some _ ->
            NewAvatar |> Failure |> resultHandler
            engine
        | None ->
            NewAvatar |> Success |> resultHandler
            engine 
            |> setAvatar (Avatar.create engine.random |> Some)
            |> generateAvatarArea

    let private transformAvatar (transform:Avatar->Avatar) (engine:Engine) : Engine =
        engine.avatar
        |> Option.fold (fun e a -> {e with avatar=a|>transform|>Some}) engine

    let private doTurnAvatarCommand (resultHandler:Result->unit) (direction:TurnDirection) (engine:Engine) : Engine =
        match engine.avatar with
        | Some a ->
            direction |> TurnAvatar |> Success |> resultHandler
            engine
            |> transformAvatar (Avatar.turn direction)
        | None ->
            direction |> TurnAvatar |> Failure |> resultHandler
            engine

    let private doMoveAvatarCommand (resultHandler:Result->unit) (direction:MoveDirection) (engine:Engine) : Engine =
        match engine.avatar with
        | Some a ->
            direction |> MoveAvatar |> Success |> resultHandler
            engine
            |> transformAvatar (Avatar.move direction)
            |> generateAvatarArea
        | None ->
            direction |> MoveAvatar |> Failure |> resultHandler
            engine

    let private doSetAvatarHomeCommand (resultHandler:Result->unit) (engine:Engine) : Engine =
        match engine.avatar with
        | Some a ->
            SetAvatarHome |> Success |> resultHandler
            engine
            |> transformAvatar (Avatar.setHome a.location)
        | None ->
            SetAvatarHome |> Failure |> resultHandler
            engine

    let private doClearAvatarHomeCommand (resultHandler:Result->unit) (engine:Engine) : Engine =
        match engine.avatar with
        | Some a ->
            ClearAvatarHome |> Success |> resultHandler
            engine
            |> transformAvatar (Avatar.clearHome)
        | None ->
            ClearAvatarHome |> Failure|> resultHandler
            engine

    let private doInteractCommand  (resultHandler:Result->unit) (engine:Engine) : Engine =
        match engine.avatar with
        | Some a ->
            let newAvatar, newArea = 
                engine
                |> getAvatarArea
                |> Area.interact resultHandler a
            engine
            |> setAvatar (newAvatar|> Some)
            |> setArea newAvatar.location newArea
             
        | None ->
            Interact |> Failure |> resultHandler
            engine

    let private doAvatarDropCommand (resultHandler:Result->unit) (item:Item) (amount:uint32) (engine:Engine) : Engine =
        match engine.avatar with
        | Some a ->
            let area =
                engine
                |> getAvatarArea
            let newAvatar, newArea =
                area
                |> Area.drop resultHandler item amount a
            engine
            |> setAvatar (newAvatar|> Some)
            |> setArea newAvatar.location newArea
        | None ->
            (item, amount) |> AvatarDrop |> Failure |> resultHandler
            engine

    let private doAvatarTakeCommand (resultHandler:Result->unit) (item:Item) (amount:uint32) (engine:Engine) : Engine =
        match engine.avatar with
        | Some a ->
            let area =
                engine
                |> getAvatarArea
            let newAvatar, newArea =
                area
                |> Area.take resultHandler item amount a
            engine
            |> setAvatar (newAvatar|> Some)
            |> setArea newAvatar.location newArea
        | None ->
            (item, amount) |> AvatarDrop |> Failure |> resultHandler
            engine

    let doCraftCommand (resultHandler:Result->unit) (recipe:Recipe) (engine:Engine) : Engine =
        match engine.avatar with
        | Some avatar ->
            let terrain = 
                (engine
                |> getAvatarArea).terrain
            if Recipe.canCraft terrain avatar.inventory recipe then
                recipe
                |> AvatarCraft
                |> Success
                |> resultHandler
                engine
                |> Map.foldBack
                    (fun k v e -> 
                        let newAvatar =
                            e.avatar.Value
                            |> Avatar.removeInventory k v
                        (k,v)
                        |> AvatarInventoryRemove
                        |> resultHandler
                        {e with avatar = newAvatar |> Some}) (recipe |> Recipe.getInputs)
                |> Map.foldBack
                    (fun k v e ->
                        let newAvatar =
                            e.avatar.Value
                            |> Avatar.addInventory k v
                        (k,v)
                        |> AvatarInventoryAdd
                        |> resultHandler
                        {e with avatar = newAvatar |> Some}) (recipe |> Recipe.getOutputs)
            else 
                recipe
                |> AvatarCraft
                |> Failure
                |> resultHandler
                engine
        | None ->
            engine

    let private updateAvatar (updater:Avatar->Avatar option) (engine:Engine) : Engine =
        {engine with avatar = engine.avatar |> Option.bind updater}

    let doEquipCommand (resultHandler:Result->unit) (slot:EquipSlot) (item:Item option) (engine:Engine) : Engine =
        match engine.avatar, item with
        | None, _ ->
            engine
        | Some _, None ->
            (slot, item)
            |> AvatarEquipSlotChange
            |> resultHandler
            engine
            |> updateAvatar (Avatar.equip slot None >> Some)
        | Some _, Some i ->
            if i |> Item.isEquippable slot then
                engine
                |> updateAvatar (Avatar.equip slot item >> Some)
            else
                engine

    let private doAvatarPlaceCommand (resultHandler:Result->unit) (engine:Engine) : Engine =
        match engine.avatar with
        | Some avatar ->
            let terrain = 
                engine
                |> getAvatarArea
                |> Area.getTerrain
            avatar.equipped
            |> Map.tryFind OnHand
            |> Option.fold 
                (fun e item ->
                    match Terrain.place item terrain with
                    | Some t ->
                        e
                        |> updateAvatarArea (Option.map (Area.setTerrain t))
                        |> updateAvatar (Avatar.removeInventory item 1u >> Some)

                    | _ ->
                        e) engine
        | None ->
            engine

    let private adjustEquipment (engine:Engine) : Engine =
        match engine.avatar with
        | Some avatar ->
            avatar.equipped
            |> Map.toList
            |> List.map
                (fun (_,i) ->
                    i, avatar.inventory |> Map.containsKey i)
            |> List.fold
                (fun e (i,f) -> 
                    if f then
                        e
                    else
                        e
                        |> updateAvatar (Avatar.unequipItem i >> Some)
                    ) engine
        | _ ->
            engine

    let doCommand (resultHandler:Result->unit) (command:Command) (engine:Engine) : Engine =
        match command with
        | AvatarDrop (_, 0u) ->
            command |> Failure |> resultHandler
            engine
        | AvatarDrop (item, amount) ->
            engine
            |> doAvatarDropCommand resultHandler item amount
        | AvatarTake (_, 0u) ->
            command |> Failure |> resultHandler
            engine
        | AvatarTake (item, amount) ->
            engine
            |> doAvatarTakeCommand resultHandler item amount
        | NewAvatar ->
            engine 
            |> doNewAvatarCommand resultHandler
        | TurnAvatar direction ->
            engine
            |> doTurnAvatarCommand resultHandler direction
        | MoveAvatar direction ->
            engine
            |> doMoveAvatarCommand resultHandler direction
        | SetAvatarHome ->
            engine
            |> doSetAvatarHomeCommand resultHandler
        | ClearAvatarHome ->
            engine
            |> doClearAvatarHomeCommand resultHandler
        | Interact ->
            engine
            |> doInteractCommand resultHandler
        | AvatarCraft recipe ->
            engine
            |> doCraftCommand resultHandler recipe
        | AvatarPlace ->
            engine
            |> doAvatarPlaceCommand resultHandler
        | AvatarEquip (slot, item) ->
            engine
            |> doEquipCommand resultHandler slot item
        |> adjustEquipment

