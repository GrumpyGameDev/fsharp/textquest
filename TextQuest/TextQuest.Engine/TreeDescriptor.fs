namespace TextQuest.Engine

type TreeDescriptor =
    {
        woodLeft: uint32
    }

module TreeDescriptor =
    let internal defaultValue =
        {
            woodLeft = 10u
        }

    let private woodLeftTable =
        [
            (3u,1u)
            (4u,3u)
            (5u,6u)
            (6u,10u)
            (7u,12u)
            (8u,12u)
            (9u,10u)
            (10u,6u)
            (11u,3u)
            (12u,1u)
        ] |> Map.ofList

    let internal create (random:System.Random) : TreeDescriptor =
        {
            woodLeft = woodLeftTable |> Utility.generate random
        }

    let internal setWoodLeft (woodLeft:uint32) (descriptor:TreeDescriptor) : TreeDescriptor =
        {descriptor with woodLeft = woodLeft}
