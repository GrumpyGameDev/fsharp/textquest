namespace TextQuest.Engine

type HomeSubcommand =
    | SetHere
    | Clear