namespace TextQuest.Engine

type Direction =
    | North
    | East
    | South
    | West
    override x.ToString() =
        match x with
        | North -> "north"
        | East -> "east"
        | South -> "south"
        | West -> "west"

module Direction =
    let list = 
        [ North; East; South; West ]

    let turn (turnDirection:TurnDirection) (direction:Direction) : Direction =
        match direction, turnDirection with
        | North, Left | South, Right | East, Around -> West
        | North, Right | South, Left | West, Around -> East
        | North, Around | East, Right | West, Left -> South
        | East, Left | South, Around | West, Right -> North

    let getMoveDirection (moveDirection:MoveDirection) (direction:Direction) : Direction =
        match moveDirection, direction with
        | Ahead, North | MoveDirection.Right, West | Back, South | MoveDirection.Left, East -> North
        | Ahead, East | MoveDirection.Right, North | Back, West | MoveDirection.Left, South -> East
        | Ahead, South | MoveDirection.Right, East | Back, North | MoveDirection.Left, West -> South
        | Ahead, West | MoveDirection.Right, South | Back, East | MoveDirection.Left, North -> West
        

