namespace TextQuest.Engine

type Avatar =
    {
        location: Location
        facing: Direction
        home: Location option
        inventory: Map<Item,uint32>
        equipped: Map<EquipSlot,Item>
    }

module Avatar =
    let internal create (random:System.Random): Avatar =
        {
            location = 
                {
                    x = random.Next()
                    y = random.Next()
                }
            facing = Direction.list |> Utility.generateFromList random
            home = None
            inventory = Map.empty
            equipped = Map.empty
        }

    let private setLocation (location:Location) (avatar:Avatar) : Avatar =
        {avatar with location = location}

    let private setFacing (facing:Direction) (avatar:Avatar) : Avatar =
        {avatar with facing = facing}

    let internal turn (direction:TurnDirection) (avatar:Avatar) : Avatar =
        avatar 
        |> setFacing (avatar.facing |> Direction.turn direction)

    let internal getLocation (avatar:Avatar) : Location =
        avatar.location

    let internal move (direction:MoveDirection) (avatar:Avatar) : Avatar =
        avatar 
        |> setLocation (avatar.location |> Location.nextLocation (avatar.facing |> Direction.getMoveDirection direction))

    let internal setHome (location:Location) (avatar:Avatar) : Avatar =
        {avatar with home = location |> Some}

    let internal clearHome (avatar:Avatar) : Avatar =
        {avatar with home = None}

    let internal getInventory (item:Item) (avatar:Avatar) : uint32 =
        match avatar.inventory |> Map.tryFind item with
        | Some x -> x
        | None -> 0u

    let private setInventory (item:Item) (amount:uint32) (avatar:Avatar) : Avatar =
        if amount > 0u then
            {avatar with inventory = avatar.inventory |> Map.add item amount}
        else
            {avatar with inventory = avatar.inventory |> Map.remove item}

    let internal addInventory (item:Item) (amount:uint32) (avatar:Avatar) : Avatar =
        avatar
        |> setInventory item (amount + (avatar |> getInventory item))

    let internal removeInventory (item:Item) (amount:uint32) (avatar:Avatar) : Avatar =
        let current = (avatar |> getInventory item) 
        if amount >= current then
            avatar |> setInventory item 0u
        else
            avatar |> setInventory item (current - amount)

    let internal equip (slot:EquipSlot) (item: Item option) (avatar:Avatar) : Avatar =
        match item with
        | None ->
            {avatar with equipped = avatar.equipped |> Map.remove slot}
        | Some i ->
            {avatar with equipped = avatar.equipped |> Map.add slot i}

    let internal unequipItem (item:Item) (avatar:Avatar) : Avatar =
        let newEquipped =
            avatar.equipped
            |> Map.filter (fun k v -> v <> item)
        {avatar with equipped = newEquipped}
