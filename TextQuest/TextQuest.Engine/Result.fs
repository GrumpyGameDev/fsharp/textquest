﻿namespace TextQuest.Engine

type Result =
    | Success               of Command
    | Failure               of Command
    | AvatarInventoryAdd    of Item      * uint32
    | AvatarInventoryRemove of Item      * uint32
    | AreaTerrainChange     of Terrain
    | AreaInventoryAdd      of Item      * uint32
    | AreaInventoryRemove   of Item      * uint32
    | AvatarEquipSlotChange of EquipSlot * (Item option)

