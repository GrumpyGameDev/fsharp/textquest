﻿namespace TextQuest.Engine

type MoveDirection =
    | Ahead
    | Left
    | Right
    | Back
    override x.ToString() =
        match x with
        | Left -> "left"
        | Right -> "right"
        | Ahead -> "ahead"
        | Back -> "back"

