﻿namespace TextQuest.Engine

type Command =
    | NewAvatar
    | MoveAvatar of MoveDirection
    | TurnAvatar of TurnDirection
    | Interact
    | SetAvatarHome
    | ClearAvatarHome
    | AvatarDrop of Item*uint32
    | AvatarTake of Item*uint32
    | AvatarCraft of Recipe
    | AvatarEquip of EquipSlot * (Item option)
    | AvatarPlace


