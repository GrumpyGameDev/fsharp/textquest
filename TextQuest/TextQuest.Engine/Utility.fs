namespace TextQuest.Engine

open System

module Utility =
    let internal generate (random:Random) (generator:Map<'TValue,uint32>) : 'TValue =
        (((0u,generator)
        ||> Map.fold (fun acc _ v -> acc + v)
        |> int
        |> random.Next
        |> uint32, None), generator)
        ||> Map.fold (fun (g,p) k v -> 
            match (g,p) with
            | (n, None) -> if n < v then (0u,Some k) else (n-v,None)
            | _         -> (g,p))
        |> snd
        |> Option.get

    let internal generateFromList (random:Random) (generator:'TValue list) : 'TValue =
        generator
        |> List.map (fun x -> (x,1u))
        |> Map.ofList
        |> generate random

