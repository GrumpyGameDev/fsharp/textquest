﻿open Lurk.Demo
open Lurk.Runner
open Lurk.DemoRunner

[<EntryPoint>]
let main argv =
    Content.create()
    |> Runner.run DemoRunner.decider DemoRunner.isRunning
    0 // return an integer exit code
