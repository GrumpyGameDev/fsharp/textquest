namespace Lurk.DemoRunner

open Lurk.Engine
open Lurk.Demo

type DemoScenario = Scenario<Direction,Location,Actor>
type DemoCommand = Command<Direction,Actor>

module DemoRunner =
    let isRunning(scenario:DemoScenario):bool = 
        true

    let playerDecider (location:Location) : DemoCommand option =
        None

    let deciderTable = 
        [(Player, playerDecider)]
        |> Map.ofList

    let decider(actor:Actor) (scenario:DemoScenario) : DemoCommand option =
        scenario.actors 
        |> Map.tryFind actor
        |> Option.bind 
            (fun instance -> None)
