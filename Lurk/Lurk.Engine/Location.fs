namespace Lurk.Engine

type Location<'TDirection,'TLocation when 'TDirection: comparison> = 
    {
        name:string
        portals: Map<'TDirection,'TLocation>
    }

module Location = 
    let create(name:string) : Location<'TDirection, 'TLocation> =
        {
            name = name
            portals = Map.empty
        }

    let setPortal (direction:'TDirection) (instance:'TLocation) (location:Location<'TDirection, 'TLocation>) : Location<'TDirection,'TLocation> =
        {location with portals = location.portals |> Map.add direction instance}
