namespace Lurk.Engine

type Scenario<'TDirection, 'TLocation, 'TActor when 'TLocation: comparison and 'TDirection: comparison and 'TActor: comparison> =
    { locations: Map<'TLocation, Location<'TDirection, 'TLocation>>
      actors: Map<'TActor, 'TLocation> }

module Scenario =
    let create(): Scenario<'TDirection, 'TLocation, 'TActor> =
        { locations = Map.empty
          actors = Map.empty }

    let setActor
        (actor: 'TActor)
        (instance: 'TLocation)
        (scenario: Scenario<'TDirection, 'TLocation, 'TActor>)
        : Scenario<'TDirection, 'TLocation, 'TActor>
        = { scenario with actors = scenario.actors |> Map.add actor instance }

    let setLocation
        (location: 'TLocation)
        (instance: Location<'TDirection, 'TLocation>)
        (scenario: Scenario<'TDirection, 'TLocation, 'TActor>)
        : Scenario<'TDirection, 'TLocation, 'TActor>
        =
        { scenario with locations = scenario.locations |> Map.add location instance }
