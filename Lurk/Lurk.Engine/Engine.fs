﻿namespace Lurk.Engine

module Engine =
    let doCommand (command:Command<'TDirection,'TActor>) (scenario:Scenario<'TDirection, 'TLocation, 'Actor>) : (Scenario<'TDirection, 'TLocation, 'Actor>) =
        scenario
