namespace Lurk.Engine

type Command<'TDirection,'TActor> =
    | Move of 'TDirection * 'TActor