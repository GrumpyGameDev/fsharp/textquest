﻿namespace Lurk.Runner

open Lurk.Engine

module Runner =
    let private doActorTurn (decider:'TActor -> Scenario<'TDirection,'TLocation,'TActor> -> (Command<'TDirection,'TActor> option)) (scenario:Scenario<'TDirection,'TLocation,'TActor>) (actor:'TActor) : Scenario<'TDirection,'TLocation,'TActor> =
        match decider actor scenario with
        | Some command ->
            scenario 
            |> Engine.doCommand command
        | _ ->
            scenario

    let rec run(decider:'TActor -> Scenario<'TDirection,'TLocation,'TActor> -> (Command<'TDirection,'TActor> option)) (isRunning:Scenario<'TDirection,'TLocation,'TActor>->bool) (scenario:Scenario<'TDirection,'TLocation,'TActor>) : unit =
        if scenario |> isRunning then
            scenario.actors
            |> Map.toList
            |> List.map fst
            |> List.fold (doActorTurn decider) scenario
            |> run decider isRunning
        else
            ()
