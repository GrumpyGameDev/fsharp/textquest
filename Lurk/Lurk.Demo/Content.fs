﻿namespace Lurk.Demo

open Lurk.Engine

module Content =
    let create() : Scenario<Direction,Location,Actor> =
        let startLocation = 
            Location.create "Start"
            |> Location.setPortal North Finish

        let finishLocation = 
            Location.create "Finish"
            |> Location.setPortal South Start
            
        Scenario.create()
        |> Scenario.setActor Player Start
        |> Scenario.setLocation Start startLocation
        |> Scenario.setLocation Finish finishLocation
        
