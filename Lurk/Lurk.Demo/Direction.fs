namespace Lurk.Demo

type Direction = 
    | North
    | East
    | South
    | West
    | Up
    | Down
    | In
    | Out

module Direction =
    let getName (direction:Direction) : string =
        match direction with
        | North -> "north"
        | East -> "east"
        | South -> "south"
        | West -> "west"
        | Up -> "up"
        | Down -> "down"
        | In -> "in"
        | Out -> "out"