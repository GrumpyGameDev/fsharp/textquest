﻿open Pathways

[<EntryPoint>]
let main argv =
    let random = System.Random()
    World.create random 20
    |> Runner.run random
    0
