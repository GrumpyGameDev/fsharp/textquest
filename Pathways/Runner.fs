namespace Pathways

module Runner = 
    let private displayStatus (random:System.Random) (world:World) : unit =
        match world.effect with
        | Nothing ->
            printfn "It's quiet around here."
        if world.pathways.ContainsKey Left then
            printfn "There is a path to the LEFT."
        if world.pathways.ContainsKey Right then
            printfn "There is a path to the RIGHT."
        if world.pathways.ContainsKey Ahead then
            printfn "There is a path AHEAD."

    let rec private inputCommand() : string list =
        printf ">"
        let input = System.Console.ReadLine()
        if input |> System.String.IsNullOrWhiteSpace |> not then
            input.ToLowerInvariant().Split(' ')
            |> List.ofArray
        else
            printfn "I don't understand what you're saying."
            inputCommand()

    [<Literal>]
    let private QuitCommand = "quit"
    [<Literal>]
    let private LeftCommand = "left"
    [<Literal>]
    let private RightCommand = "right"
    [<Literal>]
    let private AheadCommand = "ahead"
    [<Literal>]
    let private HelpCommand = "help"
    [<Literal>]
    let private JournalCommand = "journal"

    let private handleMoveCommand (random:System.Random) (direction:Direction) (world:World) : Result<World,string> =
        if world |> World.canMove then
            match world |> World.move random direction with
            | Ok w ->
                printfn "You move %s." (direction.ToString())
                w |> Ok
            | _ ->
                printfn "You cannot move that way."
                world |> Ok
        else
            Error "You cannot move at this time."

    let private showHelp (subcommand:string list) : unit =
        match subcommand with
        | [] ->
            printfn "Commands:"
            printfn "\tAHEAD   - take the path ahead, if possible."
            printfn "\tHELP    - get help on a variety of topics, or the command list."
            printfn "\tJOURNAL - get information about quests."
            printfn "\tLEFT    - take the path to the left, if possible."
            printfn "\tRIGHT   - take the path to the right, if possible."
            printfn "\tQUIT    - quits the game."
        | _ ->
            printfn "I don't know how to help you with that."

    let private showJournal (subcommand:string list) (world:World) : unit =
        match subcommand with
        | [] ->
            if world.quests.IsEmpty then
                printfn "You have no current quests."
            else
                printfn "Current Quests:"
                world.quests
                |> Map.iter
                    (fun k v -> printfn "\t%s %d" (k.ToString()) v)
        | _ ->
            printfn "I don't understand that."

    let private handleCommand (random:System.Random) (command:string list) (world:World) : Result<World,string> =
        match command with
        | JournalCommand :: tail ->
            showJournal tail world
            world |> Ok
        | [ QuitCommand ] ->
            Error "Thanks for playing!"
        | HelpCommand :: tail ->
            showHelp tail
            world |> Ok
        | [ LeftCommand ] ->
            match handleMoveCommand random Left world with
            | Error s -> 
                printfn "%s" s
                world |> Ok
            | x -> x
        | [ RightCommand ] ->
            match handleMoveCommand random Right world with
            | Error s -> 
                printfn "%s" s
                world |> Ok
            | x -> x
        | [ AheadCommand ] ->
            match handleMoveCommand random Ahead world with
            | Error s -> 
                printfn "%s" s
                world |> Ok
            | x -> x
        | _ ->
            printfn "I don't understand what you want."
            Ok world

    let rec run(random:System.Random) (world:World) : unit =
        printfn ""
        printfn "========================================"
        displayStatus random world
        match world |> handleCommand random (inputCommand()) with
        | Ok w ->
            run random w
        | _ ->
            ()
        
            
