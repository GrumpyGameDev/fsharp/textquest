namespace Pathways

type Direction =
    | Left
    | Ahead
    | Right