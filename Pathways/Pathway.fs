namespace Pathways

type Pathway =
    {
        effect: Effect
        questDeltas: Map<Quest,int>
    }

module Pathway =
    let create() : Pathway =
        {
            effect = Nothing
            questDeltas = Map.empty
        }

    let setEffect (effect:Effect) (pathway:Pathway) : Pathway =
        {pathway with effect = effect}