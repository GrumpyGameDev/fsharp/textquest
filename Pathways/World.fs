namespace Pathways

type World =
    {
        questCompletions: Set<Quest>
        quests: Map<Quest, int>
        effect: Effect
        pathways: Map<Direction,Pathway>
    }

module World =
    let private clearPathways (world:World) : World =
        {world with pathways = Map.empty}

    let private setPathway (direction:Direction) (pathway:Pathway) (world:World) : World =
        {world with 
            pathways = 
                world.pathways 
                |> Map.add direction pathway}

    let generate (random:System.Random) (world:World) : World =
        [Left; Right; Ahead] 
        |> List.sortBy (fun _ -> random.Next()) 
        |> List.take (random.Next(1,3)+random.Next(0,2))
        |> List.fold
            (fun a i -> 
                a |> setPathway i (Pathway.create() |> Pathway.setEffect Nothing)) (world |> clearPathways)

    let private setQuestCounter (quest:Quest) (value:int) (world:World) : World =
        if value>0 then
            {world with quests = world.quests |> Map.add quest value}
        else
            {world with quests = world.quests |> Map.remove quest}

    let private changeQuestCounter (quest:Quest) (delta:int) (world:World) : World * bool =
        match world.quests |> Map.tryFind quest with
        | Some value -> 
            let newValue = value + delta
            let world' = world |> setQuestCounter quest newValue
            if newValue <=0 then
                world', true
            else
                world', false
        | _ ->
            world, false


    let create(random:System.Random) (mainQuestLength:int) : World =
        {
            questCompletions = Set.empty
            quests = Map.empty
            effect = Nothing
            pathways = Map.empty
        }
        |> setQuestCounter Main mainQuestLength
        |> generate random

    let canMove(world:World) : bool =
        true

    let hasPath (direction:Direction) (world:World) : bool =
        world.pathways.ContainsKey direction

    let private setEffect (effect:Effect) (world:World) : World =
        {world with effect = effect}

    let private completeQuest (quest:Quest) (world:World) : World =
        {world with questCompletions = world.questCompletions |> Set.add quest}

    let private applyQuestDeltas (deltas:Map<Quest,int>) (world:World) : World =
        (world, deltas)
        ||> Map.fold
            (fun w q v -> 
                let w', finished = w |> changeQuestCounter q v
                if finished then
                    w'
                    |> completeQuest q
                else
                    w') 


    let move (random:System.Random) (direction:Direction) (world:World) : Result<World,string> =
        if world |> canMove && world |> hasPath direction then
            world
            |> applyQuestDeltas world.pathways.[direction].questDeltas
            |> setEffect world.pathways.[direction].effect
            |> clearPathways
            |> generate random
            |> Ok
        else
            Error "You cannot move that way."