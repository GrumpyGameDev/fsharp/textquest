﻿open DungeonDelver.ConsoleRunner

[<EntryPoint>]
let main argv =
    Runner.run()
    0
