namespace DungeonDelver.ConsoleRunner

open DungeonDelver.Engine

module Runner =
    let private showCell (engine:Engine) (row:int) (column:int) : unit =
        match engine |> Engine.tryFindCreature (column,row) with
        | Some c ->
            printf "@"
        | None ->
            match engine |> Engine.tryFindTerrain (column,row) with
            | Some Floor ->
                printf "."
            | Some Solid ->
                printf "#"
            | Some (Corner (Inside Northeast))
            | Some (Corner (Inside Southwest)) 
            | Some (Corner (Outside Northeast))
            | Some (Corner (Outside Southwest)) ->
                printf "/"
            | Some (Corner _) ->
                printf "\\"
            | Some (Wall _) -> 
                printf "*"
            | _ ->
                printf " "

    let private showRow (engine:Engine) (row:int) : unit =
        [(-3)..(3)]
        |> List.map ((+) (engine.avatar |> fst))
        |> List.iter (showCell engine row)
        printfn ""

    let private showScreen (engine:Engine) : unit =
        [(-3)..(3)]
        |> List.map ((+) (engine.avatar |> snd))
        |> List.iter (showRow engine)

    let private showStatus (engine:Engine) : unit =
        printfn "\nStatus:"
        engine
        |>showScreen

    let rec private inputMoveDirection() : Command option =
        printfn "\nMove direction?"
        printfn "[N]orth"
        printfn "[E]ast"
        printfn "[S]outh"
        printfn "[W]est"
        printfn "[Esc] Cancel"
        match System.Console.ReadKey(true).Key with
        | System.ConsoleKey.N ->
            North |> Move |> Some
        | System.ConsoleKey.E ->
            East |> Move |> Some
        | System.ConsoleKey.S ->
            South |> Move |> Some
        | System.ConsoleKey.W ->
            West |> Move |> Some
        | System.ConsoleKey.Escape ->
            None
        | _ ->
            inputMoveDirection()

    let rec private inputCommand () : Command option =
        printfn "\nWhat's next?"
        printfn "[M]ove"
        printfn "[Q]uit"
        match System.Console.ReadKey(true).Key with
        | System.ConsoleKey.Q ->
            None
        | System.ConsoleKey.M ->
            match inputMoveDirection() with
            | None ->
                inputCommand()
            | x -> x
        | _ ->
            inputCommand()

    let rec private runEngine (engine:Engine) : unit =
        engine 
        |> showStatus

        inputCommand()
        |> Option.iter 
            (fun command ->
                engine
                |> Engine.doCommand command
                |> runEngine)
        
    let run() : unit =
        Engine.create()
        |> runEngine