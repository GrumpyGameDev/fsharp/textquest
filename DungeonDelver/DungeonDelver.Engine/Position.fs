namespace DungeonDelver.Engine

type Position = int * int

module internal Position =
    let add(first:Position) (second:Position) : Position =
        ((first |> fst) + (second |> fst), (first |> snd) + (second |> snd))

    let step = Direction.delta >> add
