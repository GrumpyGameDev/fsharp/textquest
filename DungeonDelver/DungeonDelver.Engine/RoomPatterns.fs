namespace DungeonDelver.Engine


module internal RoomPatterns =
    let private table = 
        [
            '.', Floor
            '#', Solid
            'N', (Wall North)
            'E', (Wall East)
            'S', (Wall South)
            'W', (Wall West)
            '^', (Doorway North)
            '>', (Doorway East)
            'V', (Doorway South)
            '<', (Doorway West)
            'i', (Corner (Inside Northwest))
            'k', (Corner (Inside Northeast))
            'j', (Corner (Inside Southeast))
            'h', (Corner (Inside Southwest))
            'p', (Corner (Outside Northwest))
            ';', (Corner (Outside Northeast))
            'l', (Corner (Outside Southeast))
            'o', (Corner (Outside Southwest))
        ]    
        |> Map.ofList

    let private mapToTerrain (map:string list) : Map<Position,Terrain> =
        [0..(map.Length-1)]
        |> List.zip map
        |> List.map
            (fun (text, row) -> 
                let characters = text.ToCharArray() |> Array.toList
                [0..(characters.Length)]
                |> List.zip characters
                |> List.map
                    (fun (character, column) ->
                        ((column, row), table |> Map.tryFind character)))
        |> List.reduce (@)
        |> List.filter (snd >> Option.isSome)
        |> Map.ofList
        |> Map.map
            (fun k v -> v.Value)

    let chamberBase =
        [
            "uNNNNNNNNNNNNNi"
            "W.............W"
            "W.............W"
            "W.............W"
            "W.............W"
            "W.............W"
            "W.............W"
            "W.............W"
            "W.............W"
            "W.............W"
            "W.............W"
            "W.............W"
            "W.............W"
            "W.............W"
            "hSSSSSSSSSSSSSs"
        ]
        |> mapToTerrain
