namespace DungeonDelver.Engine

type CornerDirection =
    | Northeast
    | Southeast
    | Southwest
    | Northwest

type Corner =
    | Inside  of CornerDirection
    | Outside of CornerDirection

type Terrain =
    | Floor
    | Solid
    | Wall    of Direction
    | Doorway of Direction
    | Corner  of Corner