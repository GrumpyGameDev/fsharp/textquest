namespace DungeonDelver.Engine

type Engine =
    { 
        avatar: Position
        terrain: Map<Position, Terrain>
        creatures: Map<Position, Creature>
    }

type internal MovingCreature =
    {
        engine: Engine
        creature: Creature option
        position: Position
    }

module internal MovingCreature =
    let updateEngine (updater: Engine->Engine) (movingCreature:MovingCreature) : MovingCreature =
        {movingCreature with engine = movingCreature.engine |> updater}

    let updateCreature (updater: (Creature option)->(Creature option)) (movingCreature:MovingCreature) : MovingCreature =
        {movingCreature with creature = movingCreature.creature |> updater}

    let updatePosition (updater: Position->Position) (movingCreature:MovingCreature) : MovingCreature =
        {movingCreature with position = movingCreature.position |> updater}

module Engine =
    let private updateTerrain (updater:Map<Position, Terrain> -> Map<Position, Terrain>) (engine:Engine) : Engine =
        {engine with terrain = engine.terrain |> updater}

    let private updateCreatures (updater:Map<Position, Creature> -> Map<Position, Creature>) (engine:Engine) : Engine =
        {engine with creatures = engine.creatures |> updater}

    let private placeTerrain (terrain:Terrain option) (position:Position) (engine:Engine) : Engine =
        match terrain with
        | None ->
            engine
            |> updateTerrain (Map.remove position) 
        | Some t ->
            engine
            |> updateTerrain (Map.add position t)

    let private placeCreature (creature:Creature option) (position:Position) (engine:Engine) : Engine =
        match creature with
        | None ->
            engine
            |> updateCreatures (Map.remove position)
        | Some c ->
            engine
            |> updateCreatures (Map.add position c)

    let private removeAvatarCreature (engine:Engine) : Engine =
        engine
        |> placeCreature None engine.avatar

    let private placeAvatarCreature (creature:Creature option) (engine:Engine) : Engine =
        engine
        |> placeCreature creature engine.avatar

    let private tryGetCreature (position:Position) (engine:Engine) : Creature option =
        None

    let private generateTerrain(engine:Engine) : Engine =
        [for x in 0..(Constants.mapColumns-1) do 
            for y in 0..(Constants.mapRows-1) do 
                yield (x,y)]
        |> List.fold 
            (fun a i -> 
                match (i|>fst) % Constants.roomColumns, (i|>snd) % Constants.roomRows with
                | x,y when x = Constants.roomColumns-1 || y = Constants.roomRows-1 || x = 0 || y = 0 ->
                    a |> placeTerrain (Solid |> Some) i
                | x,y when x = 1 && y = 1 ->
                    a |> placeTerrain (Northwest |> Inside |> Corner |> Some) i
                | x,y when x = Constants.roomColumns-2 && y = 1 ->
                    a |> placeTerrain (Northeast |> Inside |> Corner |> Some) i
                | x,y when x = Constants.roomColumns-2 && y = Constants.roomRows - 2 ->
                    a |> placeTerrain (Southeast |> Inside |> Corner |> Some) i
                | x,y when x = 1 && y = Constants.roomRows - 2 ->
                    a |> placeTerrain (Southwest |> Inside |> Corner |> Some) i
                | x,_ when x = 1 ->
                    a |> placeTerrain (West |> Wall |> Some) i
                | x,_ when x = Constants.roomColumns-2 ->
                    a |> placeTerrain (East |> Wall |> Some) i
                | _ ->
                    a |> placeTerrain (Floor |> Some) i) engine

    let private generateAvatarCreature(engine:Engine) : Engine =
        engine
        |> placeCreature (Some Avatar) (engine.avatar)

    let private generateCreatures(engine:Engine): Engine =
        engine
        |> generateAvatarCreature

    let private generate(engine:Engine) : Engine =
        engine
        |> generateTerrain
        |> generateCreatures

    let create(): Engine = 
        { 
            avatar = (Constants.mapColumns/2,Constants.mapRows/2)
            terrain = Map.empty
            creatures = Map.empty
        }
        |> generate

    let private updateAvatar (updater:Position->Position) (engine:Engine) : Engine =
        {engine with avatar = engine.avatar |> updater}

    let private startCreatureMove (position:Position) (engine:Engine) : MovingCreature =
        {
            position = position
            engine = engine |> placeCreature None position
            creature = engine.creatures |> Map.tryFind position 
        }

    let private startAvatarMove (engine:Engine) : MovingCreature =
        engine
        |> startCreatureMove engine.avatar

    let private endCreatureMove (movingCreature:MovingCreature) : Engine =
        movingCreature.engine
        |> placeCreature movingCreature.creature movingCreature.position

    let private endAvatarMove (movingCreature:MovingCreature) : Engine =
        movingCreature
        |> endCreatureMove
        |> updateAvatar (fun _ -> movingCreature.position)

    let private handleMove (direction: Direction) = 
        startAvatarMove >> MovingCreature.updatePosition (Position.step direction) >> endAvatarMove

    let tryFindTerrain (position:Position) (engine:Engine) : Terrain option =
        engine.terrain
        |> Map.tryFind position

    let tryFindCreature (position:Position) (engine:Engine) : Creature option =
        engine.creatures
        |> Map.tryFind position

    let doCommand =
        function
        | Move direction -> handleMove direction

