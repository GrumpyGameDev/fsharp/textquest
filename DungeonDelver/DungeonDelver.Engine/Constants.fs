namespace DungeonDelver.Engine

module Constants =
    let mazeColumns = 8
    let mazeRows    = 8
    let roomColumns = 15
    let roomRows    = 15
    let mapColumns  = mazeColumns * roomColumns
    let mapRows     = mazeRows    * roomRows