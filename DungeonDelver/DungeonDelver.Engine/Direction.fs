namespace DungeonDelver.Engine

type Direction =
    | North
    | East
    | South
    | West

module internal Direction =
    let delta (direction:Direction) : int * int =
        match direction with
        | North ->
            (0,-1)
        | East ->
            (1,0)
        | South ->
            (0,1)
        | West ->
            (-1,0)
